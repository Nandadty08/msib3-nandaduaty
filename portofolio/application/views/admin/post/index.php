<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<Title>Data Pegawai</Title>
	<title></title>
	<link rel="stylesheet" href="style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Display:wght@700&family=Lexend+Deca&display=swap"
		rel="stylesheet">
</head>


<body>
	<div class="container">
		<div class="card">
			<div class="card-header">
				<h3>POST</h3>
			</div>
			<div class="card-body">
				<a href="add.php" class="btn btn-success">Tambah</a>
				<table class="table table-bordered" width="60%" id="tablePost">
					<tr>
						<th>Title</th>
						<th>Slug</th>
						<th>Category_id</th>
						<th>Content</th>
						<th>Img_feature</th>
						<th>Created_at</th>
						<th>Created_by</th>
						<th>Last_modified_time</th>
					</tr>
					<?php 
                    $no = 1; 
                    foreach($data_post as $row) 
                        {
                            echo "<tr>";
                            echo "<td>".$no."</td>";
                            echo "<td>".$row['title']."</td>";
                            echo "<td>".$row['slug']."</td>";
                            echo "<td>".$row['category_id']."</td>";
                            echo "<td>".$row['content']."</td>";
                            echo "<td>".$row['img_feature']."</td>";
                            echo "<td>".$row['created_at']."</td>";
                            echo "<td>".$row['created_by']."</td>";
                            echo "<td>".$row['last_modified_time']."</td>";
                            echo "<td><a class='btn btn-info' href='edit.php?id=".$row['id']."'>Update</a>
                            <a class='btn btn-danger' href='index.php?hapus_siswa=".$row['id']."'>Hapus</a></td>";
                            echo "</tr>";
                            $no++;
                        }
                    ?>
				</table>
			</div>
		</div>
	</div>
    <!-- Modal dialog hapus data-->
<div class="modal fade" id="myModalDelete" tabindex="-1" aria-labelledby="myModalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalDeleteLabel">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda ingin menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger" id="btdelete">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<script>
    //menampilkan data ketabel dengan plugin datatables
    $('#tablePost').DataTable();

    //menampilkan modal dialog saat tombol hapus ditekan
    $('#tablePost').on('click', '.item-delete', function() {
        //ambil data dari atribute data 
        var id = $(this).attr('data');
        $('#myModalDelete').modal('show');
        //ketika tombol lanjutkan ditekan, data id akan dikirim ke method delete 
        //pada controller mahasiswa
        $('#btdelete').unbind().click(function() {
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>post/delete/',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(response) {
                    $('#myModalDelete').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>
</body>
</html>