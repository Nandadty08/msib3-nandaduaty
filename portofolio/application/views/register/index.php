<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Halaman Register</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Display:wght@700&family=Lexend+Deca&display=swap"
		rel="stylesheet">
</head>

<body>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php 
                    if($this->session->flashdata('error') !='')
                    {
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $this->session->flashdata('error');
                        echo '</div>';
                    }
                    echo validation_errors();
                ?>
						<form method="post" action="<?php echo base_url(); ?>/register/proses">
							<h2 class="text-center">REGISTRASI</h2>
							<div class="mb-3">
								<label for="input_name" class="form-label">Nama</label>
								<input type="text" class="form-control" id="input_name" name="name"
									aria-describedby="name">

							</div>
							<div class="mb-3">
								<label for="input_phone_number" class="form-label">Phone Number</label>
								<input type="text" class="form-control" id="input_phone_number" name="phone_number"
									aria-describedby="phone_number">

							</div>
							<div class="mb-3">
								<label for="input_email" class="form-label">Email address</label>
								<input type="email" class="form-control" id="input_email" name="email"
									aria-describedby="email">

							</div>
							<div class="mb-3">
								<label for="input_username" class="form-label">Username</label>
								<input type="text" class="form-control" id="input_username" name="username"
									aria-describedby="username">

							</div>
							<div class="mb-3">
								<label for="input_password" class="form-label">Password</label>
								<input type="text" class="form-control" id="input_password" name="password">
							</div>
							<button type="submit" class="btn btn-primary">Register</button>
							<p class = " py-2 text-center">Sudah punya akun?<a href ="login">Login</a></p>
						</form>

					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

	</div>

	</div>
</body>

</html>
