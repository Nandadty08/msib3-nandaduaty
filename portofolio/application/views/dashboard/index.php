<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TASK 4 SLICING</title>
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Display:wght@700&family=Lexend+Deca&display=swap" rel="stylesheet">
</head>
<body>
    <div class="container mt-5 text-light">
        <div class="row">
          <div class="col-md-4 sedan px-5 py-5">
            <img src="<?php echo base_url()?>/assets/img/icon-sedans.svg"class="imgsedan py-4" alt="">
            <h3>SEDANS</h3>
            <p class="textsedan mt-5">
                Choose a sedan for its affordability and excelent fuel economy. 
                ideal for cruising in the city or on your next rood trip.
            </p>
            <button type="button" class="btn btn-light btn-sedan rounded-pill">Learn More</button>
          </div>
          <div class="col-md-4 suvs px-5 py-5" >
            <img src="<?php echo base_url()?>/assets/img/icon-suvs.svg" class="imgsedan py-4" alt="">
            <h3>SUVS</h3>
                <p class="textsedan mt-5">
                Take an SUV for its spacious interior, power and versatility.
                Perfect for you next family vacation and off-road adventures.
                </p>
                <button type="button" class="btn btn-light btn-suvs rounded-pill">Learn More</button>
          </div>
          <div class="col-md-4 luxury px-5 py-5"> 
            <img src="<?php echo base_url()?>/assets/img/icon-luxury.svg" class="imgsedan py-4" alt="">
            <h3>LUXURY</h3>
            <p class="textsedan mt-5">
                Curise in the best car brands without the bloated prices.
                Enjoy the enhanced comfort of a luxury rental and arrive in style.
            </p>
            <button type="button" class="btn btn-light btn-suvs rounded-pill">Learn More</button>
          </div>
        </div>
      </div>
</body>
</html>