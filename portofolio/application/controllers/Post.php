<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Post_models");
    }

    public function index()
    {
        
        $data["data_post"] = $this->Post_models->getAll();
        // var_dump($data);
        $this->load->view('post/index', $data);
       
    } 
    public function add()
    {
        $add_post = $this->post_models;
        $validation = $this->form_validation;
        $validation->set_rules($Post->rules());

        if ($validation->run()) {
            $add_post->save();
            $this->session->set_flashdata('message', '<div class= "alert alert-success alert-dismissible fade show" role="alert">
            Data Post berhasil disimpan.
            <button type ="button" class="close" data-dismis="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>>
            </button></div>');
            redirect('post');
        }

        // $data["title"] = "Tambah Data Mahasiswa";
        // $this->load->view('templates/hearder', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('mahasiswa/add', $data);
        // $this->load->view('templates/footer');
        $this->load->view('post/add.php', $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('post');

        $add_post = $this->post_models;
        $validation = $this->form_validation;
        $validation->set_rules($Post->rules());
        
        if ($validation->run()) {
            $add_post->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa berhasil disimpan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("post");
        }

        $data["title"] = "Edit Data Mahasiswa";
        $data['data_post'] = $Mahasiswa->getById($id);
        if (!$data['data_post']) show_404();
        $this->load->view('post/add.php', $data);
        // $this->load->view('templates/hearder', $data);
        // $this->load->view('templates/menu');
        // $this->load->view('mahasiswa/edit', $data);
        // $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->input->get('id');
        if (!isset($id)) show_404();
        $this->post_models->delete($id);
        $msg['success'] = true;
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Mahasiswa berhasil dihapus.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
        $this->output->set_output(json_encode($msg));
    }
}

?>