<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller{
    
    function __construct()
	{
		parent::__construct();
		$this->load->model('auth');
	}
    public function index()
    {
        
        $this->load->view('register/index.php');
        
    }
    public function proses()
	{	
		
		$this->form_validation->set_rules('name','Name', 'required');
		$this->form_validation->set_rules('phone_number', 'Number', 'required');
		$this->form_validation->set_rules('email', 'Mail', 'required');
		$this->form_validation->set_rules('username', 'User', 'required');
		$this->form_validation->set_rules('password', 'Pass', 'required');
		
		if ($this->form_validation->run()==true)
	   	{
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$this->auth->register($id,$name,$phone_number,$email,$username,$password);
			$this->session->set_flashdata('success_register','Proses Pendaftaran User Berhasil');
			redirect('login');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('register');
		}
	}
}
?>