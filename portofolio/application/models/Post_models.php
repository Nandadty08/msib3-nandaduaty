<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post_models extends CI_Model
{
    private $table = 'post';

    public function rules()
    {
        return [
            [
                'field' => 'Title',  //samakan dengan atribute name pada tags input
                'label' => 'Title',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'Slug',
                'label' => 'Slug',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'Category_id',
                'label' => 'Category_id',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'Content',
                'label' => 'Content',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'Img_feature',
                'label' => 'Img_feature',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'Created_at',
                'label' => 'Created_at',
                'rules' => 'trim|required'
            ]
            [
                'field' => 'Created_by',
                'label' => 'Created_by',
                'rules' => 'trim|required'
            ]
            [
                'field' => 'Last_modified_time',
                'label' => 'Last_modified_time',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan data post berdasarkan id post
    public function getById($id)
    {
        return $this->db->get_where($this->table, ["Id" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from post where Id='$id'
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table;
        $this->db->order_by('Id','desc');
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from post order by Id desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "Title" => $this->input->post('Title'),
            "Slug" => $this->input->post('Slug'),
            "Category_id" => $this->input->post('Category_id'),
            "Img_feature" => $this->input->post('Img_feature'),
            "Created_at" => $this->input->post('Created_at'),
            "Created_by" => $this->input->post('Created_by')
        );
        return $this->db->insert($this->table, $data);
    }

    //edit data mahasiswa
    public function update()
    {
        $data = array(
            "Title" => $this->input->post('Title'),
            "Slug" => $this->input->post('Slug'),
            "Category_id" => $this->input->post('Category_id'),
            "Img_feature" => $this->input->post('Img_feature'),
            "Created_at" => $this->input->post('Created_at'),
            "Created_by" => $this->input->post('Created_by')
        );
        return $this->db->update($this->table, $data, array('Id' => $this->input->post('Id')));
    }

    //hapus data mahasiswa
    public function delete($id)
    {
        return $this->db->delete($this->table, array("Id" => $id));
    }
}

    // function post($id,$title,$slug,$category_id,$content,$img_feature,$created_at,$created_by,$last_modified_time)
    // {
    //     $data_post = array(
    //     'id' =>'',
    //     'title'=>$title,
    //     'slug'=>$slug,
    //     'category_id'=>$category_id,
    //     'content'=>$content,
    //     'img_feature'=>$img_feature,
    //     'created_at'=>$created_at,
    //     'created_by'=>$created_by,
    //     'last_modified_time'=>$last_modified_time,
    //     );
    //     $this->db->insert('post', $data_post);
    // }

?>